// 
// ThreadLocal.cs
//  
// Author:
//       Jérémie "Garuma" Laval <jeremie.laval@gmail.com>
//       Rewritten by Paolo Molaro (lupus@ximian.com)
// 
// Copyright (c) 2009 Jérémie "Garuma" Laval
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Threading
{
	[System.Diagnostics.DebuggerDisplay ("IsValueCreated={IsValueCreated}, Value={ValueForDebugDisplay}")]
	public class ThreadLocal<T> : IDisposable
	{
		struct Datum {
			internal sbyte state; /* 0 uninitialized, < 0 initializing, > 0 inited */
			internal Exception cachedException; /* this is per-thread */
			internal T data;
		}
		
		Func<T> valueFactory;
		/* The m_data field is handled magically by the JIT
		 * It must be a struct and it is always accessed by ldflda: the JIT, instead of
		 * computing the address inside the instance, will return the address of the variable
		 * for the current thread (based on m_offset). This magic wouldn't be needed if C#
		 * let us declare an icall with a Datum& return type...
		 * For this same reason, we must check m_offset for != 0 to make sure it's valid before accessing m_data
		 * The address of the tls var is cached per method at the first IL ldflda instruction, so care must be taken
		 * not to cause it to be conditionally executed.
		 */
		uint m_offset;
		Datum m_data;
		
		public ThreadLocal ()
		{
			m_offset = 1;
		}
		
		public ThreadLocal (Func<T> valueFactory) : this ()
		{
			if (valueFactory == null)
				throw new ArgumentNullException ("valueFactory");
			this.valueFactory = valueFactory;
		}

		public void Dispose ()
		{
			Dispose (true);
		}
		
		protected virtual void Dispose (bool disposing)
		{
			if (m_offset != 0) {
				m_offset = 0;
				if (disposing)
					valueFactory = null;
				GC.SuppressFinalize (this);
			}
		}
		
		~ThreadLocal ()
		{
			Dispose (false);
		}
		
		public bool IsValueCreated {
			get {
				if (m_offset == 0)
					throw new ObjectDisposedException ("ThreadLocal object");
				/* ALERT! magic m_data JIT access redirects to TLS value instead of instance field */
				return m_data.state > 0;
			}
		}
		
		T GetSlowPath () {
			/* ALERT! magic m_data JIT access redirects to TLS value instead of instance field */
			if (m_data.cachedException != null)
				throw m_data.cachedException;
			if (m_data.state < 0)
				throw new InvalidOperationException ("The initialization function attempted to reference Value recursively");
			m_data.state = -1;
			if (valueFactory != null) {
				try {
					m_data.data = valueFactory ();
				} catch (Exception ex) {
					m_data.cachedException = ex;
					throw ex;
				}
			} else {
				m_data.data = default (T);
			}
			m_data.state = 1;
			return m_data.data;
		}
		
		[System.Diagnostics.DebuggerBrowsableAttribute (System.Diagnostics.DebuggerBrowsableState.Never)]
		public T Value {
			get {
				if (m_offset == 0)
					throw new ObjectDisposedException ("ThreadLocal object");
				/* ALERT! magic m_data JIT access redirects to TLS value instead of instance field */
				if (m_data.state > 0)
					return m_data.data;
				return GetSlowPath ();
			}
			set {
				if (m_offset == 0)
					throw new ObjectDisposedException ("ThreadLocal object");
				/* ALERT! magic m_data JIT access redirects to TLS value instead of instance field */
				m_data.state = 1;
				m_data.data = value;
			}
		}
		
		#if NET_4_5
		public IList<T> Values {
			get {
				if (m_offset == 0)
					throw new ObjectDisposedException ("ThreadLocal object");
				throw new NotImplementedException ();
			}
		}
		#endif
		
		public override string ToString ()
		{
			return Value.ToString ();
		}
		
	}
}
